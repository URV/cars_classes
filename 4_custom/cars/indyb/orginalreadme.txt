================================================================
Car name                : Ivan-Benz Phat Raca
Install in folder       : Re-volt\cars\ibphata
Author                  : Gareth Bain
Email Address           : garethbain@acticmail.com
Description             : This is a racing-car. When driving a racing-car people use THE
			  BRAKE. If U cant remember where it is it's the DOWN ARROW.
Additional Credits to   : Bubba Z for the Indy from which this is repainted. If U haven't
			  got it, get it now. U won't be disappointed.
================================================================

* Play Information *

Top speed (observed)    : 47mph
Rating                  : Pro
Results:		Supermarket 1: 12th (1:09:73 best lap) & 12th (1:10:13)
			Toys In the Hood 1  9th (1:04:633) & 11th (1:08:706)
			At Home 8th (1:13:745)  

				
* Construction *
Base                    : Indy - BUBBA Z
Editor(s) used          : Paint Shop Pro 6 & Microsoft Image Composer for the paintjob
			  & Notepad to edit the parameters of course
Known Bugs              : The wheels dont quite touth the ground. This was their in the
			  original incarnation & the only way to fix it makes the axles
			  stick above the body.


* Copyright / Permissions *

Do whatever you want with this



