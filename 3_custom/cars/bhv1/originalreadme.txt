================================================================
Car Information
================================================================
Car Name  : Leopard RT4 race truck
Car Type  : Original
Folder	  : ...\cars\catrt4
Install   : Unzip with folder names on to the main ReVolt folder
Top speed : 30 mph for Rookie, 39 mph for Pro
Drivetrain: RWD
Weight    : 4kg

================================================================
Author Information
================================================================
Author Name : Cat
EMail       : JZA80ToyotaSupraRZ@hotmail.com
Homepage    : http://www.freewebs.com/redpersiancat

================================================================
Car Description
================================================================
My first scratch made & vertex shaded body mesh. Mixed RWD with
a heavy body, so it was a pain giving the truck enough power and
making it stable at low speeds.

Design based on the Mercedes-Benz Atego Race Truck
(just used pics, no blueprints).

"Leopard" will be my division for diesels & heavy-duty, like
Saab's Scania & Mitsubishi's FUSO.

PLEASE, DO NOT IMPORT THE BODY PRM INTO ZMODELER, THAT WILL KILL
THE VERTEX SHADING!!!

================================================================
Construction
================================================================
Base           : There isn't any, it's a scratch made model.

Poly Count     : 170 for the body.
                 126 for each front wheel (x2).
                 190 for each rear wheel (x2).
                 4 for each axle (x4).

                 TOTAL POLYCOUNT: 818 polys.

Known Bugs     : None

================================================================
Programs used
================================================================
.Zmodeler 1.07b: mesh creation.
.RVshade (RV Minis 5): essential for post Zmodeler export.
.prm2hul: collision file (.hul) generator.
.Blender 2.5x: vertex shading & tris to quads conversion.
.DOSBox: for getting the Chaos RV tools to work on Win64.
.RV-sizer (Chaos RV Tools): for scaling the RC Bandit axle.
.RV-count (Chaos RV Tools): for calculating polycount.
.Ulead Photo Express 1.0: texture creation.
.GIMP 2: for fixing pureblacks and minor tweaks.
.D2Archiver: for ripping textures from Hard Truck 2.

================================================================
Thanks, Accolades & Credits
================================================================
We want to thank:
.You, for downloading this car.
.All the RV community that keeps the game alive.
.The smart people at RVZT, ORP & RVL.
.Adam, for telling me how to UV map in his GT2 conversion tut.
.Jigebren, for letting me beta test the Blender plugin, helping
  me with the vertex shading and beta testing this truck.

.Textures ripped from Hard Truck 2/King Of The Road (by
  SoftLab-NSK) & NASCAR Racing 2 (by Papyrus Design Group). 
.Rim texture from Test Drive 6 (by PitBull Syndicate).
.Chrome window texture from Bakusou Dekotora Densetsu
                                       (by HUMAN Entertainment).

Note that the door lines & shapes were made by me.

================================================================
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars,
provided you give proper credit to the creators of the parts
from this car that were used to built the new car. E.g. if
you use the wheel mesh give me credit since i did them.

Example:"Credits to Cat for the wheel mesh."
 
You MAY distribute this CAR, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, CD, etc) as long as you
include this file intact.

================================================================
Where to get this TRUCK
================================================================
Websites : www.rvzt.net
         : www.freewebs.com/redpersiancat

