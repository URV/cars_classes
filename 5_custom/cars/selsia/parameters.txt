{

;============================================================
;============================================================
; Toyota Celica Pikes Peak
;============================================================
;============================================================
Name		"Selsia Turbo"


;====================
; Models
;====================

MODEL 	0 	"cars\selsia\4.prm"
MODEL 	1 	"cars\selsia\wheel.prm"
MODEL 	2 	"NONE"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 		"cars\selsia\car.bmp"
;)TCARBOX	"cars\selsia\carbox.bmp"
;)TSHADOW	"cars\selsia\shadow.bmp"
;)SHADOWTABLE	37.5 -36.5 75.5 -65.5 -5.0
COLL 		"cars\selsia\hull.hul"
;)SFXENGINE "cars\selsia\petrol.wav"
EnvRGB 	100 100 100

;====================
; Frontend
;====================

BestTime   	TRUE
Selectable 	TRUE
;)Statistics	TRUE
Class      	1
Obtain     	0
Rating     	5
TopEnd     	3734.915527
Acc        	3.350146
Weight     	1.800000
Handling   	50.000000
Trans      	0
MaxRevs    	0.500000

;====================
; Handling
;====================

SteerRate  	3.000000
SteerMod   	0.000000
EngineRate 	10.000000
TopSpeed   	39.000000
DownForceMod	4.000000
CoM        	0.000000 -14.000000 -2.750000
Weapon     	0.000000 -16.000000 32.000000

;====================
; Body
;====================

BODY {		; Start Body
ModelNum   	0
Offset     	0, 0, 0
Mass       	1.800000
Inertia    	1300.000000 0.000000 0.000000
           	0.000000 1712.000000 0.000000
           	0.000000 0.000000 600.000000
Gravity		2200
Hardness   	0.000000
Resistance 	0.001000
AngRes     	0.001000
ResMod     	25.000000
Grip       	0.050000
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Wheels
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.00000 14.750000 39.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.400000
EngineRatio 	38000.000000
Radius      	11.500000
Mass        	0.100000
Gravity     	2200.000000
MaxPos      	9.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.035000
StaticFriction  	1.970000
KineticFriction 	1.920000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	1
Offset1  	26.00000 14.750000 39.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.400000
EngineRatio 	38000.000000
Radius      	11.500000
Mass        	0.100000
Gravity     	2200.000000
MaxPos      	9.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.035000
StaticFriction  	1.970000
KineticFriction 	1.920000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.500000 14.750000 -29.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	38000.000000
Radius      	11.500000
Mass        	0.100000
Gravity     	2200.000000
MaxPos      	9.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.040000
StaticFriction  	2.070000
KineticFriction 	1.990000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	1
Offset1  	25.500000 14.750000 -29.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	38000.000000
Radius      	11.500000
Mass        	0.100000
Gravity     	2200.000000
MaxPos      	9.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.040000
StaticFriction  	2.070000
KineticFriction 	1.990000
}          	; End Wheel


;====================
; Springs
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	11.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	11.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	11.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	11.000000
Restitution 	-0.900000
}           	; End Spring


;====================
; Pins
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Axles
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Spinner
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 0.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Aerial
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	17.000000 -19.000000 -13.000000
Direction   	0.000000 -1.000000 -1.250000
Length      	14.000000
Stiffness   	3000.000000
Damping     	2.000000
}           	; End Aerial


;====================
; AI
;====================

AI {        	 ;Start AI
UnderThresh 	207.720001
UnderRange  	1500.000000
UnderFront	450.000000
UnderRear   	386.410004
UnderMax    	0.950000
OverThresh  	100.000000
OverRange   	1403.790039
OverMax     	0.350000
OverAccThresh  	154.380005
OverAccRange   	611.049988
PickupBias     	6553
BlockBias      	6553
OvertakeBias   	22936
Suspension     	0
Aggression     	0
}           	; End AI